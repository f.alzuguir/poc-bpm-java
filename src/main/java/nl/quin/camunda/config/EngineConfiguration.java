//package nl.quin.camunda.config;
//
//import org.camunda.bpm.engine.ProcessEngine;
//import org.camunda.bpm.engine.ProcessEngineConfiguration;
//import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.InputStreamResource;
//import org.springframework.core.io.Resource;
//
//import java.io.IOException;
//import java.io.InputStream;
//
//@Configuration
//public class EngineConfiguration extends ProcessEngineConfiguration {
//
//    @Bean
//    public SpringProcessEngineConfiguration processEngineConfiguration() throws IOException {
//        SpringProcessEngineConfiguration pec = new SpringProcessEngineConfiguration();
//
//        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("gp_req_new_physexam.bpmn");
//
//        InputStreamResource resource = new InputStreamResource(inputStream);
//
//        Resource[] resources = new Resource[1];
//
//        resources[0] = resource;
//
//        pec.setDeploymentResources(resources);
//
//        return pec;
//    }
//
//    @Override
//    public ProcessEngine buildProcessEngine() {
//        return null;
//    }
//}
